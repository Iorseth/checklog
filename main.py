#!/usr/bin/python3 -B
# -*-coding:utf8 -*

# IMPORT

from pylogweb import * # bibliothèque pyweblog
from platform import system
import argparse
import sys
import os
import glob
from pprint import pprint

def usage() :
  """
  help
  """
  print('Usage: main_argparse.py [-v|--verbose] [-o|--output=filename]')
  print('  filename = input file')
  print('  -h | --help = help')
  print('  -v | --verbose = verbose mode')
  print('  -o | --output=filename = output file')
  exit()

def getargs(args):
  """
  recupère les arguments du script
  """
  # création du parser
  parser = argparse.ArgumentParser(description='get arguments')
  # arguments facultatifs avec paramètre
  parser.add_argument('-o','--output', dest='output', default='output', help='output file')
  parser.add_argument('-d','--logdir', dest='logdir', help='input logdir')
  parser.add_argument('-f','--logfile', dest='logfile', help='input logfile')
  return parser.parse_args()

# VARIABLES

rawloglist = [] # liste qui contiendra la liste des fichiers log brut
separator = None

# EXECUTION DU SCRIPT

if __name__ == '__main__' :

# vérification premier niveau


  # récupération depuis arguments
  args = getargs(sys.argv)
  logfile = args.logfile
  logdir = args.logdir
  output = args.output

  # test si Linux
  if system()!= 'Linux' :
    print("platform.system() =! 'Linux'")
    print('This script only works with good OS. Sorry, not sorry.')
    exit(0)

  # validation des arguments
  # 2 arguments : erreur
  if logdir != None and logfile != None :
    print('Wrong argument')
    print('Choose either [--logfile] or [--logdir], not both')
    exit(3)
  # 0 argument passé : valeur par defaut
  if logdir == None and logfile == None :
    rawloglist = dirtolistlog(deflogdir())
  else :
    # un argument passé : lequel ? valide ?
    # logdir
    if logdir != None :
      # test si dossier et si contient des fichiers logs accessibles
      if os.path.isdir(logdir) :
        rawloglist = dirtolistlog(os.path.realpath(logdir))
      else :
        print(logdir, " doesn't seems to be a directory")
        exit(1)
    # logfile
    elif os.path.isfile(logfile):
      if os.access(logfile,4):
        rawloglist = [ os.path.realpath(logfile)]
    else:
      print(logfile, " is not a file or is not readable")
      exit(1)

  # rawloglist contient des fichiers logs ?
  if len(rawloglist) == 0 :
    print("The log directory contain no readable files ending with log")
    exit(1)
  
# Checkpoint : liste des fichiers à analyser 
  print('Files that will be analyzed :')
  pprint(rawloglist)

# création de la liste préformaté

  # séparateur de champ
  #separator = findsep(firstline(rawloglist))
  print('separator : ->{}<-'.format(separator))
