#!/usr/bin/python3 -B

from basic_functions import checkfile
import os
import numpy as np



def count_ips(apache_log_path):
    """ La fonction trie les IP en fonction du nombre de fois 
    où elles apparaissent dans les logs
    Retourne une liste de tuples sous la forme
    [('IP1', nb_occurences_x), ('IP2', nb_occurences_y)...]
    """

    path, my_log = checkfile(apache_log_path)
    os.chdir(path)
    
    
    with open(my_log) as f:
        log_lines = f.readlines()
    # On créé une liste en splittant et on garde le 1er élément (l'ip)
    ip_list = [x.split()[0] for x in log_lines]

    # On compte le nombre d'occurences pour chaque ip et on met ça dans un dictonnaire
    dict_ips = {}
    for i in ip_list:
        if i in dict_ips:
            dict_ips[i] += 1
        else:
            dict_ips[i] = 1
   # On trie le dictionnaire non pas sur la clé, mais sur la valeur
    return sorted(dict_ips.items(), key=lambda x:x[1])


def count_max_ip(apache_log_path):
    """ Cette fonction va trouver l'IP qui apparait le plus dans les logs Apache et 
    retourner un dictionnaire sous la forme 
    {"IP": "occurences"} 
    :apache_log_path = str"""
    path, my_log = checkfile(apache_log_path)
    os.chdir(path)
    
    
    with open(my_log) as f:
        log_lines = f.readlines()
    
    # On créé une liste en splittant et on garde le 1er élément (l'ip)
    ip_list = [x.split()[0] for x in log_lines]

    # On compte le nombre d'occurences pour chaque ip et on met ça dans un dictonnaire
    dict_ips = {}
    for i in ip_list:
        if i in dict_ips:
            dict_ips[i] += 1
        else:
            dict_ips[i] = 1

    # On créé un dictionnaire avec les clés/valeurs dans ip mais on ne garde que
    # la clé qui a la valeur maximale dans le dictionnaire
    
    return {k:v for k, v in dict_ips.items() if v == max(dict_ips.values())}



print(count_ips('/Users/benjamin.debaisieux/Documents/git/checklog/sample_logs/apache_logs1'))
print(count_max_ip('/Users/benjamin.debaisieux/Documents/git/checklog/sample_logs/apache_logs1'))

    


