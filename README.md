# PYLOGWEB

## Description

```
pylogweb.py
```

Utilitaire écrit en Python permettant d'analyser les fichiers de logs des serveurs web.

### Contraintes et environnement

- GNU/Linux
- Compatible Python 2.6.6 --> 3.5 (test sur environnement virtuel)
- Pas de dépendance Python requise (sauf modules déjà installé comme os, re, ...)

### Liste code erreur d'exit()

- 0 = normal
- 1 = fichier source manquant ou non lisible
- 2 = formatage inconnu du fichier log, impossible de le parser
- 3 = problème dans les arguments

### Fonctionnalité attendue

- travaille sur : dossier par défaut / dossier spécifié / fichier source

**FAIT dans main.py**

```
# dossier de log par défaut
pylogweb.py options1

# dossier de log spécifié
pylogweb.py -d archive/log/httpd/ options1
pylogweb.py --log-dir="archive/log/httpd/" options1

# fichier log spécifié
pylogweb.py -f fichier.log options1
pylogweb.py --log-file="fichier.log" options1

```

`exit(1)` à la moindre erreur (fichiers/dossier manquant ou non lisible)

- dossier par défaut

serveur web pris en charge minimum = Apache HTTPD
OS pris en charge minimum = Debian 9 et dérivés (Alexis), rhel 7 et dérivés (Cyprien)
OS à tester = Debian 8 et dérivés , rhel 6 et dérivés (cyprien)

**FAIT dans pyweblog**
```
def defdirlog() :
  # tester si existe e: 
  #   return '/var/log/apache2' # apache debian
  #   return '/var/log/httpd'   # apache sur rhel
  #   return '/var/log/httpd24' # apache sur rhel via SCL
  #   exit(1)                      # si rien
# utiliser logdirdefault() si ni -f ni -d spécifié
```

- sépatateur de champ par defaut

**EN COURS (Cyprien)**
```
def find_sep(string 1er ligne fichier) :
  # supprime contenu entre les double quotes et simple quote
  # supprime les quotes 
  # uniq
  # si count(char_restant) == 1 alors char_restant = séparateur
  # sinon exit(2)
```

- Un seul argument de type input (logdir / logfile)


**FAIT main.py (Cyprien)**

exit(3)

- parser de champ


pour chaque champ trouvé grace au séparateur test si c'est un champ de type
Le parser renvoit ensuite une liste de dictionnaire avec des champs contenant du string/int ou rien (on supprime le - des valeurs nulles)

```
def parser_column(string fichier):
 # return list_log
 
# list_log = [ 
{ 'h' = '8.8.8.8'},
{ 'l' = ''},
{ 'u' = ''},
{ 't' = 'date formaté'}]
```

liste des fonction de type : is_champ(str champ, int position_champ, int max_champ) et get_champ

```
is_h() # IP ou fqdn
is_l() # pseudo ou -
is_u() # pseudo ou -
is_t() # [22/Apr/2018:05:28:11 +0200]
get_t() # renvoit la date formaté en jj/mm/YYY HH:MM:ss
is_r() # début GET/POST/UPDATE/OPTIONS ... fin HTTP/1.1 ou autre version
is_s() # (pour %s et %>s) int 1xx 2xx 3xx 4xx 5xx
is_b() # int ou -
is_refereri() # de type url ou - (par défaut c'est le dernier -1 élement d'une ligne et il est entre guillemet)
is_useragenti() # debut Mozilla/... sinon - ( par défaut c'est le dernier élément d'une ligne et il est entre guillemet)
get_useragenti() # renvoit uniquement "nom_navigateur" "version" 
```

- compteur d'IP source

```
def count_ip(list_log)
  return int
```


## Information utile et Source

### Information utile

#### Emplacement par défaut des fichiers d'Apache Httpd

- apache2.conf / httpd.conf

```
/etc/apache2/apache2.conf
/etc/httpd/conf/httpd.conf
```

- log apache
```
/var/log/apache2 # debian
/var/log/httpd   # rhel
/var/log/httpd24 # apache 2.4 rhel via SCL (Software Collection est un dépot officiel contenant davantage de version des logiciels)
```

- notation de formatage apache

https://httpd.apache.org/docs/2.4/fr/mod/mod_log_config.html

```
%h = hôte distant (IP sinon FQDN )
%l = identifiant via identd sinon "-"
%u = identifiant via auth sinon "-" ou false si 401
%t = temps par défaut ( exemple : [22/Apr/2018:05:28:11 +0200] )
%{format}t = temps au format spécifié
%r = première ligne de la requête (contenant la commande GET,PUT,POST,... les éléments en paramètres et la version du protocole HTTP comme HTTP/1.1 )
%s = code de statut de la requête
%>s =  code de statut de la dernière requête si redirection interne
%b = taille de la réponse en byte sinoon "-"
%{Referer}i = url source avant click si indiqué sinon "-"
%{User-Agent}i = navigateur du client envoyé par le navigateur ( peut contenir des commentaires)

# exemple avec le CustomLog combined
%h          %l%u%t                           "%r                                                                        " %>s %b"%{referer}i""%{User-Agent}i"
88.88.88.88 - - [22/Apr/2018:05:28:11 +0200] "GET /ddd/aaa/20180420/mobile/5aEq3CkWeX_DX144_D      Y4_C2_CY.png HTTP/1.1" 304 - "-" "Mozilla/4.0 (compatible;)"
%h          %l%u%t                           "%r                      " %>s %b  "%{referer}i""%{User-Agent}i"
88.88.88.88 - - [25/Apr/2018:22:31:33 +0200] "GET /robots.txt HTTP/1.1" 404 298 "-" "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"
%h        %l%u    %t                           "%r                         " %>s %b   "%{referer}i                      " "%{User-Agent}i                   "
127.0.0.1 - frank [10/Oct/2000:13:55:36 -0700] "GET /apache_pb.gif HTTP/1.0" 200 2326 "http://www.example.com/start.html" "Mozilla/4.08 [en] (Win98; I ;Nav)" 
```
- exemple de formatage spécifique de log et nommage

```
# LogFormat et nommage des CustomLog
LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
LogFormat "%h %l %u %t \"%r\" %>s %b" common
LogFormat "%{Referer}i -> %U" referer
LogFormat "%{User-agent}i" agent
LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio

# exemple d'appel des format
CustomLog logs/access_vhost1_log combined
```

### Source

Des projets similaires existent : 

- Compteur d'IP source sur log apache : https://medium.com/devops-challenge/apache-log-parser-using-python-8080fbc41dda
- bibliothèque installable facilement mais utilisable après un BAC+7 : https://github.com/rory/apache-log-parser

Des projets propriétaires de stats en temps réel existent aussi avec création de graph et webgui mais le but premier n'est pas le temps réel. Par contre une webgui en Flask peut être intéressante.
