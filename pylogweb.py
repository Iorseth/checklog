#! /usr/bin/python3 -B
# -*-coding:utf8 -*

# BLIBLIOTHEQUE DE FONCTIONS INDEPENDANTES

import os.path

def deflogdir() :
  """
  renvoit le premier dossier de log du serveur web trouvé sur le système
  """
  # liste des dossiers par defaut des serveurs web
  defdirlog = [
    '/var/log/apache2/',
    '/var/log/httpd/',
    '/var/log/httpd24',
    'sample_logs' # valeur de test
    ]
  for logdir in defdirlog :
    if os.path.isdir(logdir):
      return os.path.realpath(logdir)
  # dirlog not found
  print("It seem there's no log dir in the system")
  exit(1)

def dirtolistlog(logdir):
  """
  renvoit une liste de fichier *log du dossier
  """
  list=[]
  # ajout des fichiers log à la liste rawloglist
  for file in os.listdir(logdir):
    file=os.path.join(logdir,file)
    if file.endswith("log") and os.path.isfile(file) and os.access(file,4):
      list.append(os.path.realpath(file))
  return list

def firstline(listfile):
  """
  renvoit la première ligne du premier fichier de la liste
  """
  with open(listfile[0]) as f:
    return f.readline().rstrip()
  

#def find_sep(line) :
  # supprime contenu entre les double quotes et simple quote
  # supprime les quotes 
  # uniq
  # si count(char_restant) == 1 alors char_restant = séparateur
  # sinon exit(2)
  #line
